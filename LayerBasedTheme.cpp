#include "LayerBasedTheme.h"

#include "kaleidoscope/layers.h"
#include "kaleidoscope/plugin/LEDControl.h"

namespace kaleidoscope {
namespace plugin {
EventHandlerResult LayerBasedTheme::onLayerChange() {
  uint8_t current_layer = Layer.mostRecent();
  uint8_t current_mode = ::LEDControl.get_mode_index();
  uint8_t new_mode;

  if (current_layer <= 3) {
    new_mode = 2;
  } else {
    new_mode = 3;
  }

  if (new_mode != current_mode) {
    ::LEDControl.set_mode(new_mode);
  }

  return EventHandlerResult::OK;
}

LayerBasedTheme::TransientLEDMode::TransientLEDMode(
    const LayerBasedTheme *parent)
    : parent_(parent){};
} // namespace plugin
} // namespace kaleidoscope

kaleidoscope::plugin::LayerBasedTheme LayerBasedTheme;
