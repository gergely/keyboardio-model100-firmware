#pragma once

#include "kaleidoscope/event_handler_result.h"
#include "kaleidoscope/plugin.h"
#include "kaleidoscope/plugin/AccessTransientLEDMode.h"
#include "kaleidoscope/plugin/LEDMode.h"
#include "kaleidoscope/plugin/LEDModeInterface.h"

namespace kaleidoscope {
namespace plugin {
class LayerBasedTheme : public Plugin,
                        public LEDModeInterface,
                        public AccessTransientLEDMode {
public:
  LayerBasedTheme(void) {}

  EventHandlerResult onLayerChange();

  class TransientLEDMode : public LEDMode {
  public:
    explicit TransientLEDMode(const LayerBasedTheme *parent);

  private:
    const LayerBasedTheme *parent_;
  };
};
} // namespace plugin
} // namespace kaleidoscope

extern kaleidoscope::plugin::LayerBasedTheme LayerBasedTheme;
