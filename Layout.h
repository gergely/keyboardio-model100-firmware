enum {
  PRIMARY,
  SHIFTED,
  ALTGR,
  ALTGR_SHIFT,
  FUNCTION,
  HUN,
  COLEMAK,
  ONEHAND_LEFT,
  ONEHAND_RIGHT,
};

enum {
    LEAD_EXT_PY,
    LEAD_EXT_ORG,
    LEAD_INIT_PY,
};

enum {
    MAIN,
};

/* This comment temporarily turns off astyle's indent enforcement
 *   so we can make the keymaps actually resemble the physical key layout better
 */
// clang-format off

KEYMAPS(
  [PRIMARY] = KEYMAP_STACKED
  (___,         Key_1,           Key_2,     Key_3,      Key_4, Key_5, Key_LEDEffectNext,
   RALT(Key_7), LSHIFT(Key_6),   Key_Comma, Key_Period, Key_P, Key_Z, TD(0),
   ___,         Key_A,           Key_O,     Key_E,      Key_U, Key_I,
   ___,         RALT(Key_Comma), Key_Q,     Key_J,      Key_K, Key_X, Key_Tab,
   Key_LeftControl, Key_Backspace, OSL(SHIFTED), Key_Escape,
   OSL(FUNCTION),

   LEAD(MAIN), Key_6, Key_7, Key_8, Key_9, Key_Backtick, MoveToLayer(HUN),
   TD(1),      Key_F, Key_G, Key_C, Key_R, Key_L,        LSHIFT(Key_7),
               Key_D, Key_H, Key_T, Key_N, Key_S,        LSHIFT(Key_1),
   Key_Enter,  Key_B, Key_M, Key_W, Key_V, Key_Y,        Key_Slash,
   Key_LeftGui, Key_Enter, Key_Spacebar, OSM(LeftAlt),
   OSL(ALTGR)),

  [SHIFTED] = KEYMAP_STACKED
  (___,         LSHIFT(Key_4),      RALT(Key_V),   RALT(Key_X),      RALT(Key_Semicolon), LSHIFT(Key_5), ___,
   RALT(Key_1), LSHIFT(Key_Comma),  RALT(Key_M),   RALT(Key_Period), LSHIFT(Key_P),       LSHIFT(Key_Z), LSHIFT(Key_Tab),
   ___,         LSHIFT(Key_A),      LSHIFT(Key_O), LSHIFT(Key_E),    LSHIFT(Key_U),       LSHIFT(Key_I),
   ___,         LSHIFT(Key_Period), LSHIFT(Key_Q), LSHIFT(Key_J),    LSHIFT(Key_K),       LSHIFT(Key_X), ___,
   ___, LSHIFT(Key_Backspace), ___, ___,
   ___,

   ___,               RALT(Key_3),   RALT(Key_C),   RALT(Key_Slash), LSHIFT(Key_8), LSHIFT(Key_9), MoveToLayer(COLEMAK),
   LSHIFT(Key_Enter), LSHIFT(Key_F), LSHIFT(Key_G), LSHIFT(Key_C),   LSHIFT(Key_R), LSHIFT(Key_L), LSHIFT(Key_3),
                      LSHIFT(Key_D), LSHIFT(Key_H), LSHIFT(Key_T),   LSHIFT(Key_N), LSHIFT(Key_S), LSHIFT(Key_2),
   ___,               LSHIFT(Key_B), LSHIFT(Key_M), LSHIFT(Key_W),   LSHIFT(Key_V), LSHIFT(Key_Y), LSHIFT(Key_Slash),
   ___, OSL(ALTGR_SHIFT), ___, ___,
   ___),

  [ALTGR] = KEYMAP_STACKED
  (___,                XXX,             RALT(Key_2),     RALT(LSHIFT(Key_3)),        RALT(Key_U),                    RALT(Key_5),                     ___,
   RALT(Key_Backtick), RALT(Key_A),     Key_0,           RALT(LSHIFT(Key_Period)),   Key_Minus,                      RALT(Key_Z),                     ___,
   ___,                Key_Quote,       Key_Equals,      Key_Semicolon,              Key_RightBracket,               Key_NonUsBackslashAndPipe,
   ___,                RALT(Key_Quote), Key_LeftBracket, RALT(Key_J),                Key_Backslash,                  RALT(Key_NonUsBackslashAndPipe), ___,
   ___, ___, ___, OSL(ALTGR_SHIFT),
   ___,

   ___, XXX,                 XXX,         RALT(LSHIFT(Key_Slash)), RALT(Key_8),         RALT(LSHIFT(Key_8)),     MoveToLayer(ONEHAND_LEFT),
   ___, RALT(Key_I),         RALT(Key_L), RALT(Key_E),             RALT(Key_H),         RALT(Key_S),             RALT(Key_RightBracket),
        RALT(Key_D),         XXX,         RALT(Key_T),             RALT(Key_K),         RALT(Key_Minus),         RALT(Key_Equals),
   ___, RALT(LSHIFT(Key_O)), RALT(Key_O), RALT(Key_P),             RALT(LSHIFT(Key_M)), RALT(LSHIFT(Key_Comma)), RALT(Key_Y),
   OSL(ALTGR_SHIFT), ___, ___, ___,
   ___),

  [ALTGR_SHIFT] = KEYMAP_STACKED
  (___,         XXX,                      RALT(LSHIFT(Key_2)),     XXX,                   RALT(Key_Backslash),      RALT(LSHIFT(Key_5)),                     ___,
   RALT(Key_R), RALT(LSHIFT(Key_A)),      LSHIFT(Key_0),           XXX,                   LSHIFT(Key_Minus),        RALT(LSHIFT(Key_Z)),                     ___,
   ___,         LSHIFT(Key_Quote),        LSHIFT(Key_Equals),      LSHIFT(Key_Semicolon), LSHIFT(Key_RightBracket), LSHIFT(Key_NonUsBackslashAndPipe),
   ___,         RALT(LSHIFT(Key_Quote)),  LSHIFT(Key_LeftBracket), RALT(LSHIFT(Key_J)),   LSHIFT(Key_Backslash),    RALT(LSHIFT(Key_NonUsBackslashAndPipe)), ___,
   ___, ___, ___, ___,
   ___,

   ___, RALT(LSHIFT(Key_C)), RALT(LSHIFT(Key_R)), RALT(LSHIFT(Key_V)), RALT(LSHIFT(Key_Semicolon)), XXX,                     ___,
   ___, RALT(LSHIFT(Key_I)), RALT(LSHIFT(Key_L)), RALT(LSHIFT(Key_E)), RALT(LSHIFT(Key_H)),         RALT(LSHIFT(Key_S)),     RALT(LSHIFT(Key_4)),
        RALT(LSHIFT(Key_D)), XXX,                 RALT(LSHIFT(Key_T)), RALT(LSHIFT(Key_K)),         RALT(LSHIFT(Key_Minus)), RALT(LSHIFT(Key_Equals)),
   ___, RALT(LSHIFT(Key_7)), RALT(LSHIFT(Key_B)), RALT(LSHIFT(Key_N)), RALT(LSHIFT(Key_0)),         RALT(Key_0),             RALT(LSHIFT(Key_Y)),
   ___, ___, ___, ___,
   ___),

  [FUNCTION] = KEYMAP_STACKED
  (Key_PrintScreen, Key_F1,          Key_F2,      Key_F3,        Key_F4,        Key_F5,            Key_CapsLock,
   Key_Tab,         ___,             Key_mouseUp, Key_mouseBtnN, Key_mouseBtnR, Key_mouseScrollDn, Key_PageUp,
   Key_Home,        Key_mouseL,      Key_mouseDn, Key_mouseR,    Key_mouseBtnL, Key_mouseScrollUp,
   Key_End,         Key_PrintScreen, Key_Insert,  Key_mouseBtnP, Key_mouseBtnM, ___,               Key_PageDown,
   ___, Key_Delete, ___, Key_LeftShift,
   ___,

   Consumer_ScanPreviousTrack, Key_F6,                 Key_F7,                   Key_F8,                   Key_F9,         Key_F10,     Key_F11,
   Consumer_PlaySlashPause,    Consumer_ScanNextTrack, RALT(Key_B),              RALT(Key_N),              RALT(Key_F),    RALT(Key_G), Key_F12,
   ___,                        Key_LeftArrow,          Key_DownArrow,            Key_UpArrow,              Key_RightArrow, RALT(Key_X),
   Key_PcApplication,          Consumer_Mute,          Consumer_VolumeDecrement, Consumer_VolumeIncrement, ___,            RALT(Key_Q), RALT(Key_W),
   Key_RightShift, Key_RightAlt, Key_Enter, ___,
   ___),

  [HUN] = KEYMAP_STACKED
  (___,          Key_1, Key_2, Key_3, Key_4, Key_5, Key_LEDEffectNext,
   Key_Backtick, Key_Q, Key_W, Key_E, Key_R, Key_T, Key_Tab,
   ___,          Key_A, Key_S, Key_D, Key_F, Key_G,
   ___,          Key_Z, Key_X, Key_C, Key_V, Key_B, Key_Escape,
   Key_LeftControl, Key_Backspace, Key_LeftAlt, Key_LeftShift,
   OSL(FUNCTION),

   ___,         Key_6, Key_7, Key_8,     Key_9,      Key_0,         MoveToLayer(PRIMARY),
   Key_Enter,   Key_Y, Key_U, Key_I,     Key_O,      Key_P,         Key_Minus,
                Key_H, Key_J, Key_K,     Key_L,      Key_Semicolon, Key_Quote,
   Key_LeftGui, Key_N, Key_M, Key_Comma, Key_Period, Key_Slash,     Key_Backslash,
   Key_RightShift, Key_RightAlt, Key_Spacebar, Key_RightControl,
   OSL(FUNCTION)),

  [COLEMAK] = KEYMAP_STACKED
  (___,                       Key_1, Key_2, Key_3, Key_4, Key_5, ___,
   Key_Backtick,              Key_Q, Key_W, Key_F, Key_P, Key_G, Key_Tab,
   ___,                       Key_A, Key_R, Key_S, Key_T, Key_D,
   Key_NonUsBackslashAndPipe, Key_Y, Key_X, Key_C, Key_V, Key_B, Key_Escape,
   Key_LeftControl, Key_Backspace, Key_LeftAlt, Key_LeftShift,
   OSL(FUNCTION),

   ___,         Key_6, Key_7, Key_8,     Key_9,      Key_0,         MoveToLayer(PRIMARY),
   Key_Enter,   Key_J, Key_L, Key_U,     Key_Z,      Key_Semicolon, Key_Equals,
                Key_H, Key_N, Key_E,     Key_I,      Key_O,         Key_Quote,
   Key_LeftGui, Key_K, Key_M, Key_Comma, Key_Period, Key_Slash,     Key_Minus,
   Key_RightShift, Key_RightAlt, Key_Spacebar, Key_RightControl,
   OSL(FUNCTION)),

  [ONEHAND_LEFT] = KEYMAP_STACKED
  (XXX, Key_1, Key_2, Key_3, Key_4, Key_5, Key_Tab,
   XXX, Key_Q, Key_W, Key_E, Key_R, Key_T, Key_Enter,
   XXX, Key_A, Key_S, Key_D, Key_F, Key_G,
   XXX, Key_Z, Key_X, Key_C, Key_V, Key_B, Key_Escape,
   Key_LeftControl, Key_Spacebar, Key_LeftAlt, Key_LeftShift,
   OSL(ONEHAND_RIGHT),

   XXX, XXX, XXX, XXX, XXX, XXX, XXX,
   XXX, XXX, XXX, XXX, XXX, XXX, XXX,
        XXX, XXX, XXX, XXX, XXX, XXX,
   ___, XXX, XXX, XXX, XXX, XXX, XXX,
   XXX, XXX, XXX, XXX,
   XXX),

  [ONEHAND_RIGHT] = KEYMAP_STACKED
  (___, Key_6, Key_7, Key_8,     Key_9,      Key_Backtick,  MoveToLayer(PRIMARY),
   ___, Key_Y, Key_U, Key_I,     Key_O,      Key_P,         ___,
   ___, Key_H, Key_J, Key_K,     Key_L,      Key_Semicolon,
   ___, Key_N, Key_M, Key_Comma, Key_Period, Key_Slash,     ___,
   Key_LeftControl, Key_Backspace, Key_LeftAlt, Key_LeftShift,
   ___,

   XXX, XXX, XXX, XXX, XXX, XXX, XXX,
   XXX, XXX, XXX, XXX, XXX, XXX, XXX,
        XXX, XXX, XXX, XXX, XXX, XXX,
   ___, XXX, XXX, XXX, XXX, XXX, XXX,
   XXX, XXX, XXX, XXX,
   XXX)
) // KEYMAPS(

/* Re-enable astyle's indent enforcement */
// clang-format on
