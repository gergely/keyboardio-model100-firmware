#include <Kaleidoscope.h>

#include "TapDance.h"

namespace gpolonkai {
namespace TapDance {
bool cancelOneShot = false;
}
}

void
tapDanceAction(
    uint8_t tap_dance_index,
    KeyAddr key_addr,
    uint8_t tap_count,
    kaleidoscope::plugin::TapDance::ActionType tap_dance_action
)
{
    switch (tap_dance_index) {
        case LEFT_PARENS:
            return tapDanceActionKeys(
                tap_count,
                tap_dance_action,
                LSHIFT(Key_8), RALT(Key_F), RALT(Key_B)
            );
        case RIGHT_PARENS:
            return tapDanceActionKeys(
                tap_count,
                tap_dance_action,
                LSHIFT(Key_9), RALT(Key_G), RALT(Key_N)
            );
        case OPENING_QMS:
            return tapDanceActionKeys(
                tap_count,
                tap_dance_action,
                // “                   „             ‘                      ‛                      «                      ‹
                LSHIFT(RALT(Key_O)), RALT(Key_O), LSHIFT(RALT(Key_M)), LSHIFT(RALT(Key_7)), LSHIFT(RALT(Key_N)), RALT(Key_0)
            );
        case CLOSING_QMS:
            return tapDanceActionKeys(
                tap_count,
                tap_dance_action,
                // ”          ’                          »                      ›
                RALT(Key_P), LSHIFT(RALT(Key_Comma)), LSHIFT(RALT(Key_B)), LSHIFT(RALT(Key_0))
            );
    }
}
