#pragma once

#include <Kaleidoscope-TapDance.h>

enum {
    LEFT_PARENS,
    RIGHT_PARENS,
    OPENING_QMS,
    CLOSING_QMS,
};

extern void
tapDanceAction(uint8_t tapDanceIndex, KeyAddr key_addr, uint8_t tapCount, kaleidoscope::plugin::TapDance::ActionType tapDanceAction);

namespace gpolonkai {
namespace TapDance {
    extern bool cancelOneShot;
}
}
