// -*- mode: c++ -*-
// Copyright 2016-2022 Keyboardio, inc. <jesse@keyboard.io>
// See "LICENSE" for license details

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built on " __DATE__ " at " __TIME__
#endif

#include "Kaleidoscope.h"

#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"

#include "Kaleidoscope-FocusSerial.h"

#include "Kaleidoscope-MouseKeys.h"

#include "Kaleidoscope-Macros.h"

#include "Kaleidoscope-LEDControl.h"

#include "Kaleidoscope-LEDEffect-BootGreeting.h"

#include "Kaleidoscope-LEDEffect-Rainbow.h"

#include "Kaleidoscope-LED-Stalker.h"

#include "Kaleidoscope-LED-Palette-Theme.h"

#include "Kaleidoscope-Colormap.h"

#include "Kaleidoscope-HardwareTestMode.h"

#include "Kaleidoscope-HostPowerManagement.h"

#include "Kaleidoscope-MagicCombo.h"

#include "Kaleidoscope-USB-Quirks.h"

#include "Kaleidoscope-OneShot.h"
#include "Kaleidoscope-Escape-OneShot.h"

#include "Kaleidoscope-LED-ActiveModColor.h"

#include "Kaleidoscope-TapDance.h"

#include "TapDance.h"

#include "Kaleidoscope-Leader.h"

#include "LayerBasedTheme.h"

#include "Layout.h"

/** The Model 100's key layouts are defined as 'keymaps'.
 *
 * Each keymap is defined as a list using the 'KEYMAP_STACKED' macro, built
 * of first the left hand's layout, followed by the right hand's layout.
 *
 * Keymaps typically consist mostly of `Key_` definitions. There are many, many
 * keys defined as part of the USB HID Keyboard specification. You can find the
 * names (if not yet the explanations) for all the standard `Key_` defintions
 * offered by Kaleidoscope in these files:
 *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs/keyboard.h
 *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs/consumerctl.h
 *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs/sysctl.h
 *    https://github.com/keyboardio/Kaleidoscope/blob/master/src/kaleidoscope/key_defs/keymaps.h
 *
 * Additional things that should be documented here include
 *   using ___ to let keypresses fall through to the previously active layer
 *   using XXX to mark a keyswitch as 'blocked' on this layer
 *   using ShiftToLayer() and LockLayer() keys to change the active keymap.
 *   keeping NUM and FN consistent and accessible on all layers
 *
 * A key defined as 'ShiftToLayer(FUNCTION)' will switch to FUNCTION while held.
 * Similarly, a key defined as 'LockLayer(FUNCTION)' will switch to FUNCTION
 * when tapped.
 */

#define MACRO_TYPER(name, string) \
    static void name##Typer(uint8_t seq_index) { \
        Macros.type((string));                   \
    }

#define LEAD_MACRO(name) name##Typer

// Note that this layout was made for a Hungarian OS layout, hence the y/z and _/? swaps.
MACRO_TYPER(extPython, ".pz");
MACRO_TYPER(extOrg, ".org");
MACRO_TYPER(pyInit, "??init??");

static const kaleidoscope::plugin::Leader::dictionary_t leader_dictionary[] PROGMEM = LEADER_DICT
    ({LEADER_SEQ(LEAD(MAIN), Key_P), LEAD_MACRO(extPython)},
     {LEADER_SEQ(LEAD(MAIN), Key_O), LEAD_MACRO(extOrg)},
     {LEADER_SEQ(LEAD(MAIN), Key_I), LEAD_MACRO(pyInit)});

void toggleLedsOnSuspendResume(
    kaleidoscope::plugin::HostPowerManagement::Event event) {
  switch (event) {
  case kaleidoscope::plugin::HostPowerManagement::Suspend:
    LEDControl.disable();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Resume:
    LEDControl.enable();
    break;
  case kaleidoscope::plugin::HostPowerManagement::Sleep:
    break;
  }
}

void hostPowerManagementEventHandler(
    kaleidoscope::plugin::HostPowerManagement::Event event) {
  toggleLedsOnSuspendResume(event);
}

enum {
  COMBO_TOGGLE_NKRO_MODE,
  COMBO_ENTER_TEST_MODE
};

static void toggleKeyboardProtocol(uint8_t combo_index) {
  USBQuirks.toggleKeyboardProtocol();
}

static void enterHardwareTestMode(uint8_t combo_index) {
  HardwareTestMode.runTests();
}

USE_MAGIC_COMBOS({.action = toggleKeyboardProtocol,
                  // Left Fn + Esc + Shift
                  .keys = {R3C6, R2C6, R3C7}},
                 {.action = enterHardwareTestMode,
                  // Left Fn + Prog + LED
                  .keys = {R3C6, R0C0, R0C6}});

KALEIDOSCOPE_INIT_PLUGINS(
  EEPROMSettings,
  EEPROMKeymap,
  Focus,
  FocusSettingsCommand,
  FocusEEPROMCommand,
  BootGreetingEffect,
  HardwareTestMode,
  LEDControl,
  LEDOff,
  LEDRainbowWaveEffect,
  StalkerEffect,
  LEDPaletteTheme,
  ActiveModColorEffect,
  ColormapEffect,
  Macros,
  MouseKeys,
  HostPowerManagement,
  MagicCombo,
  USBQuirks,
  OneShot,
  EscapeOneShot,
  EscapeOneShotConfig,
  TapDance,
  Leader,
  LayerBasedTheme
);

void setup() {
  Kaleidoscope.setup();
  LEDRainbowWaveEffect.brightness(255);
  HardwareTestMode.setActionKey(R3C6);
  StalkerEffect.variant = STALKER(BlazingTrail);
  StalkerEffect.activate();
  EEPROMKeymap.setup(9);
  ColormapEffect.max_layers(9);
  Leader.dictionary = leader_dictionary;
  MouseKeys.speed = 2;
  MouseKeys.setSpeedLimit(31);
  MouseKeys.wheelDelay = 60;
}

void loop() {
  Kaleidoscope.loop();
}
